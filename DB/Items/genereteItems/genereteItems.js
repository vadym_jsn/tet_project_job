const faker = require('faker');
const {knex} = require("../../indexDB")



function getRandomPrice(min, max) {
   return Math.floor(Math.random() * (max - min + 1)) + min; 
 }

const generationItems = (amountItems) =>{
   const items = []
   for (let i = 0; i < amountItems; i++) {
   
      const objItems = {
         item_name: faker.internet.userName(),
         price: getRandomPrice(100, 500)
      }
      
      items.push(objItems)
   }

   return items
}

const CreateTableItems = async (req, res, next) =>{
   try {
      let isTableCrated = await knex.schema.createTable('Items', (table) => {
         table.increments('iid')
         table.string('item_name')
         table.integer('price')

     })
     if(isTableCrated){
        res.status(200).json({info: "table was created"})
     }
   } catch (error) {
         console.log('error at function CreateTableItems', error );
      }
}

const insertDataItems =  async (req, res, next)=> {
   let items = generationItems(2)


   try {
      let isPartners = await knex('Items').insert(items)
      res.status(200).json({info: "Items data inserted"})
   } catch (error) {
      console.log("error at function insertDataItems", error)
   }
}

module.exports ={
   CreateTableItems,
   insertDataItems
}