const faker = require('faker');
const {knex} = require("../../indexDB")



const randomDataRegistration = (minYearRegist, maxYearRegist) =>{
   function getRandomArbitrary(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    let date1 =new Date(`01/01/${minYearRegist}`).getTime()
    let date2 = new Date(`01/01/${maxYearRegist + 1}`).getTime()
        return new Date(getRandomArbitrary(date1, date2)) 
    
}

function getRandomIp(min, max) {
   return Math.floor(Math.random() * (max - min + 1)) + min; 
 }

const genereteIpForUser = (firstInitIp, userRange, firstIpInt) => {
   return `${firstInitIp}.${getRandomIp(0, 255)}.${getRandomIp(0,255)}.${getRandomIp(0,255)}`
}

const getRandomActiveNOActive = (minRange, maxRange) =>{
   let numberIgn = Math.sign(getRandomIp(minRange, maxRange))

   if(numberIgn == 1 || 0){
      return true
   }else{
      return false
   }

   
}


const devideUsers = (chunk, ArrUsers) =>{
let devidedUsers = []
   for (let i = 0; i < ArrUsers.length ;  i += chunk) {
      devidedUsers.push(ArrUsers.slice(i, i + chunk))
      
   }
   return devidedUsers
}

const generationUsers = (amountUsers, quantityRangeUser) =>{
let users = []
let userNumber 
let userRange = quantityRangeUser 
let firstInitIp = getRandomIp(0, 255);
   for (let i = 0; i < amountUsers; i++) {
      userNumber = i
      if(userRange === userNumber){
         userRange += quantityRangeUser
            firstInitIp = getRandomIp(0, 255)
      }
      const ObjUser = {
         login: faker.internet.userName(),
         password: faker.internet.password(),
         registration_date:randomDataRegistration(2017, 2018),
         last_visit_date: randomDataRegistration(2018, 2018),
         ip: genereteIpForUser(firstInitIp),
         isActive: getRandomActiveNOActive(-255, 255)
      }


      users.push(ObjUser)
   }

   let devidedUsers = devideUsers(20000, users)
   return devidedUsers
}


const CreateTableUsers = async (req, res, next) =>{
try {
   let isTableCrated = await   knex.schema.createTable('Users', (table) => {
      table.increments('uid')
      table.string('login')
      table.string('password')
      table.string('registration_date')
      table.string('last_visit_date')
      table.string('ip')
      table.boolean('isActive')
  })

  if(isTableCrated){
     res.status(200).json({info: "table was created"})
  }
} catch (error) {
      console.log('error at function CreateTableUsers', error );
         
   }
}


const inserDataUsers = async (req, res, next) =>{
   let users = generationUsers(200000, 10000)
   for (let i = 0; i < users.length; i++) {
      try {
         await knex('Users').insert(users[i])
      } catch (error) {
         console.log('error at function inserDataUsrs', error);
      }
   }

    res.status(200).json({info: 'users were inserted'})
}
module.exports = {
   generationUsers,
   CreateTableUsers,
   inserDataUsers,
}