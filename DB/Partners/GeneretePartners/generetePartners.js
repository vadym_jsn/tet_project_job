const faker = require('faker');
const {knex} = require("../../indexDB")


const generationPartners = (amountPartners) => {
   const partners = []
   for (let i = 0; i < amountPartners; i++) {

      const objPartners = {
         partner_name: faker.internet.userName(),
      }
      
      partners.push(objPartners)
   }

   return partners
}




const CreateTablePartners= async (req, res, next) =>{
   try {
      let isTableCrated = await knex.schema.createTable('Partners', (table) => {
         table.increments('pid')
         table.string('partner_name')


     })
     if(isTableCrated){
        res.status(200).json({info: "table was created"})
     }
   } catch (error) {
         console.log('error at function CreateTablePartners', error );
      }
}

const insertDataPartners =  async (req, res, next)=>{
   let partners= generationPartners(10)
   try {
      let isPartners = await knex('Partners').insert(partners)
      res.status(200).json({info: "partners data inserted"})
   } catch (error) {
      console.log("error at function insertDataPartners", error)
   }
}


   module.exports = {
      CreateTablePartners,
      insertDataPartners
   }