const faker = require('faker');
const {knex} = require("../../indexDB")

const createTableGroup = async (req, res, next) => {
try {
   let isTableCrated = await knex.schema.createTable('Group', (table) => {
 table.increments('gid')
 table.string('Group')
      // table.string('temporary')
      // table.string('regular')
      // table.string('editors')
      // table.string('admin')

   // table.foreign('gid').references('uid').inTable('Users')
   // ?? как сделать foregin key for that table

  })

  res.status(200).json({info: "table created"})
} catch (error) {
   console.log('error at function createTableGroup', error);

   res.status(500).json({info: 'error'})
   }
}

const generationGroups = () =>{
   const Namegroups = ['temporary', 'regular', 'editors','admin']
   const groups = []
try {
   for (let i = 0; i < Namegroups.length; i++) {
      const objGroups = {
         Group: Namegroups[i],
      }
      groups.push(objGroups)
   }

   return groups

} catch (error) {
   console.log('error at function createGroupsTable', error);
   }

}

const insertGroupsTable = async (req, res, next) => {
   try {
      let groups = generationGroups()

      let isPartners = await knex('Group').insert(groups)
      res.status(200).json({info: "Group data inserted"})
   } catch (error) {
      console.log('error at function insertGroupsTable', error);
   }
}



module.exports = {
   createTableGroup,
   insertGroupsTable
}