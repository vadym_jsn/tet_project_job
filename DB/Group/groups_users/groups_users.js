const faker = require('faker');
const {knex} = require("../../indexDB")


const createGroup_users = async (req, res, next) => {
   try {
      await knex.schema.createTable('Groups_users', (table)=>{
         table.integer('user_id').unsigned().nullable()
         table.integer('group_id')
         table.foreign('user_id').references('uid').inTable('Users')
      })

      res.status(200).json({info: "table was created"})
   } catch (error) {
   res.status(500).json({info: "error"})
   console.log('error at function createGroup_users', error);
   
   }
}


const determineCountGroups = (min, max) =>{
   return Math.floor(Math.random() * (max - min + 1)) + min; 
}


const devidegroups_users= (chunk, arr) =>{
   let devidedArr = []
      for (let i = 0; i < arr.length ;  i += chunk) {
         devidedArr.push(arr.slice(i, i + chunk))
         
      }
      return devidedArr
   }

const gener_DataGroups_Users = async () =>{
try {
   let usersID = await  knex.select("uid").from('Users')
   let groupId = await knex.select('gid').from('Group');
   let adminId = await  knex('Group').where({
      Group: "admin" 
    }).select('gid')

   let groups_users = []
   let isAdmin = false
   let CountGroups = groupId.length

   for (let i = 0; i < usersID.length; i++) {
      const CountBelongGroups = determineCountGroups(1, CountGroups)

      if(CountBelongGroups === adminId[0].gid && !isAdmin){
         const objForAdmin = {
            user_id: usersID[i].uid,
            group_id: adminId[0].gid
         }
         CountGroups = groupId.length -1
         isAdmin = true
         groups_users.push(objForAdmin)
      }else{
         for (let j = 0; j < CountBelongGroups; j++) {
            const obj = {
               user_id: usersID[i].uid,
               group_id: groupId[j].gid
            }
               groups_users.push(obj)
            }
      }


   }

   let result = devidegroups_users(20000, groups_users)
   
   return result

} catch (error) {
   console.log('error', error);
   
}

}

const inserDataGroups_Users = async (req, res, next) =>{
   let users = await gener_DataGroups_Users()
   for (let i = 0; i < users.length; i++) {
      try {
         await knex('Groups_users').insert(users[i])
      } catch (error) {
         console.log('error at function inserDataUsrs', error);
      }
   }

    res.status(200).json({info: 'data were inserted'})
}

module.exports = {
   gener_DataGroups_Users,
   createGroup_users,
   inserDataGroups_Users
}