const faker = require('faker');
const {knex} = require("../../indexDB")


const createTeblesOrders = async (req, res, next) =>{
   try {
         let isTableCreated = await knex.schema.createTable('Orders', (table)=>{
            table.integer('uid').unsigned()
            table.integer('pid').unsigned().nullable()
            table.integer('iid').unsigned()

            table.foreign('uid').references('uid').inTable('Users')
            table.foreign('pid').references('pid').inTable('Partners')
            table.foreign('iid').references('iid').inTable('Items')
         })

         res.status(200).json({info: 'table was created'})
   } catch (error) {
      console.log('error at func createTeblesOrders', error)
   }
}


function getRandomIndex(min, max) {
   return Math.floor(Math.random() * (max - min + 1)) + min; 
 }





const generateDataOrders = async (amountOrders) =>{
   const devidedOrders = (chunk, arr) =>{
      let devidedArr = []
         for (let i = 0; i < arr.length ;  i += chunk) {
            devidedArr.push(arr.slice(i, i + chunk))
         }
         return devidedArr
   }
   let usersID = await  knex.select("uid").from('Users')
   let partnersID = await knex.select("pid").from("Partners")
   let itemsID = await knex.select('iid').from("Items")
   let orders = []
   try {

      for (let i = 0; i < amountOrders; i++) {
         let randomIndexUsers = getRandomIndex(0, 200000 - 1)   
         let randomIndexPartners = getRandomIndex(0, 15);
         let randomIndexItems = getRandomIndex(0, 1)
         
         let objOrders = {
            uid: usersID[randomIndexUsers].uid,
            pid: partnersID[randomIndexPartners] ? partnersID[randomIndexPartners].pid : null,
            iid: itemsID[randomIndexItems].iid
         }
         orders.push(objOrders)
      }
      let result = devidedOrders(20000, orders)
      return result
   } catch (error) {
      console.log('error at func generateDataOrders', error)
   }
}



const insertDataOrders = async (req, res, next) =>{
   let orders = await generateDataOrders(500000)
try {
   for (let i = 0; i < orders.length; i++) {
      await knex('Orders').insert(orders[i])
   } 
   res.status(200).json({info: "data was inserted"})
} catch (error) {
   console.log('error at function inserDataUsrs', error);
   }
}




module.exports = {
   createTeblesOrders,
   insertDataOrders
}