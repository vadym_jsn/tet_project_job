
require("dotenv").config();


const knex = require('knex')({
   client: 'mysql',
   connection: {
     host : process.env.host,
     user : process.env.userName,
     password : process.env.userPassword,
     database : 'default'
   },
 });
 const bookshelf = require("bookshelf")(knex);

 module.exports = {bookshelf, knex}