const express = require("express");
const Router_generateTables = express.Router();
const testRoutre = express.Router()
const {CreateTableUsers, inserDataUsers} = require("../../DB/users/genereteUsers/genereteUsers")
const {CreateTablePartners, insertDataPartners}  =require("../../DB/Partners/GeneretePartners/generetePartners")
const {CreateTableItems, insertDataItems} = require("../../DB/Items/genereteItems/genereteItems")
const {createTableGroup, insertGroupsTable} = require("../../DB/Group/genereteGroup/genereteGroup")
const {createGroup_users, inserDataGroups_Users} = require("../../DB/Group/groups_users/groups_users")
const {createTeblesOrders, insertDataOrders} = require("../../DB/Orders/genereteOrders/genereteOrders")



Router_generateTables.route("/createuserTable")
.get(CreateTableUsers)
.post(inserDataUsers)


Router_generateTables.route("/createPartnersTable")
.get(CreateTablePartners)
.post(insertDataPartners)

Router_generateTables.route("/createItemsTable")
.get(CreateTableItems)  
.post(insertDataItems)

Router_generateTables.route('/createGroupTable')
.get(createTableGroup)
.post(insertGroupsTable)

Router_generateTables.route('/createGroups_users')
.get(createGroup_users)
.post(inserDataGroups_Users)

Router_generateTables.route('/createOrders')
.get(createTeblesOrders)
.post(insertDataOrders)


module.exports = {Router_generateTables, testRoutre}